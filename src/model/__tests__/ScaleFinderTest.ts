import { Note, NoteLiteral } from "@tonaljs/tonal";
import ScaleFinder from "../ScaleFinder";
import consts from "./ScaleFinderTestConsts.json"

const scaleFinder = new ScaleFinder();

function resolveNoteNames(positions: NoteLiteral[][][]) {
  return positions.map(string => {
    return string.map(position => {
      return position.map(note => Note.simplify(Note.get(note).name));
    })
  })
}
test('scale finder returns violin positions by default', () => {
  expect(resolveNoteNames(scaleFinder.getPositionsForTuning())).toStrictEqual(consts.violinPositions);
});

test('scale finder returns viola positions when given a different tuning', () => {
  expect(resolveNoteNames(scaleFinder.getPositionsForTuning(["C3", "G3", "D4", "A4"]))).toStrictEqual(consts.violaPositions);
});

test('scale finder creates accurate notes for a position given a starting root', () => {
  expect(scaleFinder.createPositionNotes("G3", 7).map(note => {return Note.simplify(Note.get(note).name)}))
    .toStrictEqual(["G3", "Ab3", "A3", "Bb3", "B3", "C4", "Db4"]);
});

test('scale finder correctly reports the range of a given tuning', () => {
  expect(scaleFinder.getRangeForTuning(scaleFinder.getPositionsForTuning()).map(note => Note.get(note).name)).toStrictEqual(["G3", "Eb7"]);
});

test('mapScaleToFingerboard should return a veiw of the fingerboard with only the notes of the G major scale included', () => {
  const fingerboard = scaleFinder.getPositionsForTuning();
  expect(resolveNoteNames(scaleFinder.mapScaleToFingerboard({fingerboard, root:"G",name: "major"}))).toStrictEqual(consts.gScaleToFingerboard);
})

test('mapScaleToFingerboard should return a veiw of the fingerboard with only the notes of the F# major scale included', () => {
  const fingerboard = scaleFinder.getPositionsForTuning();
  expect(resolveNoteNames(scaleFinder.mapScaleToFingerboard({fingerboard, root:"F#",name: "major"}))).toStrictEqual(consts.fSharpScaleToFingerboard);
})