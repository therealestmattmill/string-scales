import { Interval, Note, NoteLiteral, Scale, ScaleType } from "@tonaljs/tonal";

const createArray = (length: Number) => [...Array(length)];

//First position is transposed 0 semitones
//Second position is transposed 3 semitones
const positionTransposeSemitones = [0, 3, 5];

export const roots = ["G", "G#/Ab", "A", "A#/Bb", "B", "C", "C#/Db", "D", "D#/Eb", "E", "F", "F#/Gb"];
export const scales = ScaleType.all()
    .map(scaleType => scaleType.name)
    .sort();

class ScaleFinder {

    /**
     * For each note/string in the tuning array, return a list of positions, 
     * each position containing a list of every note in that position.
     * See ScaleFinderTest for an example of the output
     * @param {*} tuning 
     */
    getPositionsForTuning(tuning = ["G3", "D4", "A5", "E6"]) {
        let rootNotes = tuning.map(note => Note.get(note))
        return rootNotes.map(rootNote => {
            return positionTransposeSemitones.map((semitones) => {
                return this.createPositionNotes(Note.transpose(rootNote, Interval.fromSemitones(semitones)), semitones === 0 ? 8 : 7);
            });
        });
    }

    /**
     * Returns a tuple containing the lowest and highest note for this tuning.
     * @param {*} tuning 
     */
    getRangeForTuning(tuning:NoteLiteral[][][]) {
        let lowest = tuning[0][0][0];
        let highString = tuning[tuning.length - 1];
        let highPosition = highString[highString.length - 1];
        let highNote = highPosition[highPosition.length - 1];
        return [lowest, highNote];
    }

    /**
     * Helper method which returns an array containing every note above the rootNote up to
     * numNotes number of notes.
     * createPositionNotes("G3", 7) === ["G3", "Ab3", "A3", "Bb3", "B3", "C4", "Db4"]
     * @param {*} rootNote 
     * @param {*} numNotes 
     */
    createPositionNotes = (rootNote: NoteLiteral, numNotes: Number) => {
        return createArray(numNotes).map((_, tones) => {
            return Note.get(Note.simplify(Note.transpose(Note.get(rootNote), Interval.fromSemitones(tones))));
        });
    }

    /**
     * Calculates the scale notes over the fretboard range, then applies that scale
     * to the fingerboard, filtering out notes that are not in the scale.
     * 
     * @returns a fingerboard containing only the notes in the scale on a given string / position.
     */
    mapScaleToFingerboard = ({ fingerboard, root, name }:
                            {fingerboard: NoteLiteral[][][], root: string, name: string}) => {
        const [fromNote, toNote] = this.getRangeForTuning(fingerboard);
        const scaleRangeFunc: (fromNote: string, toNote: string) => (string|undefined)[] = 
            Scale.rangeOf(root + " " + name);
        const scaleRange = scaleRangeFunc(Note.get(fromNote).name, Note.get(toNote).name)
            .filter(noteStr => noteStr !== undefined)
            .map(noteStr => Note.get(noteStr!!));

        return fingerboard.map(string => {
            return string.map(position => {
                return scaleRange
                    .filter(note => position.some(element => {
                        return this.equalNotes(note, element);
                    }))
            })
        })
    }

    /**
     * Helper function to compare Note objects
     * @param {Note} note1 
     * @param {Note} note2 
     */
    equalNotes = (note1: NoteLiteral, note2: NoteLiteral) => 
        Note.octave(note1) === Note.octave(note2) &&
        Note.chroma(note1) === Note.chroma(note2)
}

export default ScaleFinder;