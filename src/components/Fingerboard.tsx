import React from "react";
import FingerboardDisplayBase from "./FingerboardDisplayBase";
import FingerboardNotes from "./FingerboardNotes";

export default function Fingerboard() {
    return (
        <>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                viewBox="0 0 200 300"
                className="fullWidth">
                <FingerboardDisplayBase />
                <FingerboardNotes />
            </svg>
        </>
    )
}
