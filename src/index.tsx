import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './App.css'
import App from './App';
import ScaleOptionsContextProvider from "./components/providers/ScaleContextProvider"

ReactDOM.render(
  <React.StrictMode>
    <ScaleOptionsContextProvider>
      <App />
    </ScaleOptionsContextProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
