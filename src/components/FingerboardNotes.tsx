import React, {useContext} from "react";
import { ScaleOptionsContext } from "./providers/ScaleContextProvider";
import ScaleFinder from "../model/ScaleFinder";
import StringDisplay from "./StringDisplay";

let stringPositions = [60, 86, 114, 140];

export default function FingerboardNotes() {
    const { selectedScaleOptions } = useContext(ScaleOptionsContext);
    const scaleFinder = new ScaleFinder();
    const fingerboard = scaleFinder.getPositionsForTuning();
    const scaleSelection = scaleFinder.mapScaleToFingerboard({fingerboard, ...selectedScaleOptions});
    return (
        <>
            {scaleSelection.map( (string, idx) => {
                return <StringDisplay notes={string} position={stringPositions[idx]} fingerboard={fingerboard[idx]}/>
            })}
        </>
    )
}
