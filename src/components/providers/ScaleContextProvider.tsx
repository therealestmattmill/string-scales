import React, { createContext, useState } from "react";
import {roots, scales} from "../../model/ScaleFinder";

const scaleOptions = {roots, scales};
const defaultValue = {scaleOptions, selectedScaleOptions: {name: "major", root: "G"}, setRoot: function (f: string) :void {}, setScale: function (f: string) :void {}};
export const ScaleOptionsContext = createContext(defaultValue);

export default function ScaleOptionsContextProvider({ children }:{children: React.ReactNode}) {
    const [selectedScaleOptions, setSelectedScaleOptions] = useState({name: "major", root: "G"});
    const setRoot = (newRoot: string) => {
        setSelectedScaleOptions({root:newRoot, name:selectedScaleOptions.name});
    };

    const setScale = (name: string) => {
        setSelectedScaleOptions({root: selectedScaleOptions.root, name});
    };
    
    return (
        <ScaleOptionsContext.Provider value={{scaleOptions, selectedScaleOptions, setRoot, setScale}}>
            {children}
        </ScaleOptionsContext.Provider>
    )
}
