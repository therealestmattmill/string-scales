import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import Jumbotron from 'react-bootstrap/Jumbotron';
import ScaleSelection from "./components/ScaleSelection"
import Fingerboard from "./components/Fingerboard"

function App() {

return <Container className="p-3">
  <Jumbotron>
    <h1 className="header">String Scales</h1>
  </Jumbotron>
  <Container className ="body">
    <Row>
      <Col xs="4" className="sidebar">
        <ScaleSelection />
      </Col>
      <Col xs="8" className="main" >
        <Fingerboard />
      </Col>
    </Row>
  </Container>
</Container>
}

export default App;