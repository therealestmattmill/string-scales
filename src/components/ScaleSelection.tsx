import React, {useContext} from "react";
import ScaleRootList from "./ScaleRootList";
import ScaleQualityList from "./ScaleQualityList"
import {ScaleOptionsContext} from "./providers/ScaleContextProvider";

export default function ScaleQualityListEntry() {
    const {selectedScaleOptions} = useContext(ScaleOptionsContext);

    return (
        <div>
            <h4> Current Scale:</h4>
            {selectedScaleOptions.root} {selectedScaleOptions.name}   
            <h5>Root</h5>
            <ScaleRootList />
            <h5>Scale Quality</h5>
            <ScaleQualityList />
        </div>
    )
}