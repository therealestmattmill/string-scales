import React, {useContext} from "react";
import {ScaleOptionsContext} from "./providers/ScaleContextProvider";

const ScaleQualityListEntry = ({name, setSelected = f => f} : {name: string, setSelected: (f: string) => void}) => 
    <div onClick={() => setSelected(name)}>{name}</div>

export default function ScaleQualityList() {
    const {scaleOptions, setScale} = useContext(ScaleOptionsContext);

    return (
        <>
            {
            scaleOptions.scales.map(scaleName =>
                <ScaleQualityListEntry setSelected={setScale} name={scaleName}/>)
            }
        </>
    );
}