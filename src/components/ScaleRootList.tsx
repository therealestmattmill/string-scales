import React, {useContext} from "react";
import {ScaleOptionsContext} from "./providers/ScaleContextProvider";

const ScaleRoot = ({root = "G", setSelected = f=>f}: {root: string, setSelected: (f: string) => void}) => 
    <span onClick = {() => setSelected(root)}>{root+" "}</span>


const EnharmonicEquivalentScaleRoots = ({root = "F#/Gb", setSelected= f=>f}: {root: string, setSelected: (f: string) => void}) => (
    <div>
        <ScaleRoot root={root.slice(0, 2)} setSelected={setSelected} />/ <ScaleRoot root={root.slice(3, 5)} setSelected={setSelected} />
    </div>
)

export default function ScaleRootList() {
    const {scaleOptions, setRoot} = useContext(ScaleOptionsContext);
    return (
        <>  
            <div>{
            scaleOptions.roots.map(root =>
                {
                    return root.includes("/") ? 
                    <EnharmonicEquivalentScaleRoots root={root} setSelected={setRoot} /> : 
                    <div><ScaleRoot root={root} setSelected={setRoot} /></div>
            })}
            </div>
        </>
    );
}