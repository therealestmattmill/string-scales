import React from "react";

export default function FingerboardDisplayBase() {
    return (
        <>
            <defs>
                <path
                    id="db6aR796e"
                    d="M158.79 21c4.53 0 8.21 3.68 8.21 8.21v243.58c0 4.53-3.68 8.21-8.21 8.21H41.21c-4.53 0-8.21-3.68-8.21-8.21V29.21c0-4.53 3.68-8.21 8.21-8.21h117.58z"
                ></path>
                <mask
                    id="maskbJcdXju5j"
                    width="142"
                    height="268"
                    x="29"
                    y="17"
                    maskUnits="userSpaceOnUse">
                    <path fill="#fff" d="M29 17H171V285H29z"></path>
                    <use xlinkHref="#db6aR796e"></use>
                </mask>
                <path id="a5VoNwZW9K" d="M60 284V20"></path>
                <path id="e28kVZv1rF" d="M86 284V20"></path>
                <path id="a20Top2kTM" d="M114 284V20"></path>
                <path id="b130j9dYpQ" d="M140 284V20"></path>
                <path id="d4ZEFX4uQA" d="M167 75H33"></path>
                <path id="fjtRcbsod" d="M167 125H33"></path>
                <path id="ff7HptV3Z" d="M167 154.69H33"></path>
                <path id="bjlXPOmrC" d="M167 205H33"></path>
            </defs>
            <use fill="#454545" xlinkHref="#db6aR796e"></use>
            <g mask="url(#maskbJcdXju5j)">
                <use
                    fillOpacity="0"
                    stroke="#242424"
                    strokeWidth="4"
                    xlinkHref="#db6aR796e"
                ></use>
            </g>
            <use
                fillOpacity="0"
                stroke="#e2e2e2"
                strokeWidth="3"
                xlinkHref="#a5VoNwZW9K"
            ></use>
            <use
                fillOpacity="0"
                stroke="#e2e2e2"
                strokeWidth="3"
                xlinkHref="#e28kVZv1rF"
            ></use>
            <use
                fillOpacity="0"
                stroke="#e2e2e2"
                strokeOpacity="0.93"
                strokeWidth="3"
                xlinkHref="#a20Top2kTM"
            ></use>
            <use
                fillOpacity="0"
                stroke="#e2e2e2"
                strokeOpacity="0.93"
                strokeWidth="3"
                xlinkHref="#b130j9dYpQ"
            ></use>
            <use
                fillOpacity="0"
                stroke="#ed1c24"
                strokeWidth="3"
                xlinkHref="#d4ZEFX4uQA"
            ></use>
            <use
                fillOpacity="0"
                stroke="#863cbf"
                strokeWidth="3"
                xlinkHref="#fjtRcbsod"
            ></use>
            <use
                fillOpacity="0"
                stroke="#00aeef"
                strokeWidth="3"
                xlinkHref="#ff7HptV3Z"
            ></use>
            <g>
                <use
                    fillOpacity="0"
                    stroke="#11ed44"
                    strokeWidth="3"
                    xlinkHref="#bjlXPOmrC"
                ></use>
            </g>
        </>
    );
}