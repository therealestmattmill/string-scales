import {Note, NoteLiteral} from "@tonaljs/tonal"
import React from "react";
import ScaleFinder from "../model/ScaleFinder";

let yPositions = [20, 50, 75, 100, 125, 155, 180, 205, 230];
let scaleFinder = new ScaleFinder();

const CircleWithNoteName = ({cx, cy, note}: {cx: number, cy: number, note: string}) => 
    <>
    <circle cx={cx} cy={cy} r="12" className="first"></circle>
    <text x={cx-4} y={cy+2} font-family="Verdana" font-size="10" fill="white">
        {note}
    </text>
    </>

export default function StringDisplay({notes, position, fingerboard}: 
                                    {notes: (NoteLiteral)[][], position: number, fingerboard: (NoteLiteral)[][]}) {
        return (
        <>
        {
        notes[0].map(note => {
            let compareFn = (elementToCompare: NoteLiteral) => {
                return scaleFinder.equalNotes(elementToCompare, note);
            }

            let notePosition = fingerboard[0].findIndex(compareFn, note);
            return <CircleWithNoteName cx={position} cy={yPositions[notePosition]} note={Note.get(note).pc}/>
        })}
        </>
    )
}