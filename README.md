## String Scale Finder
String Scale Finder is a React app that displays scale positions for stringed instruments (Violin only currently).

Uses [tonal.js](https://github.com/tonaljs/tonal) for all music theory related data.

Uses SVG for the fingerboard and note rendering.

Use npm run start to start the app in dev mode.